package com.nx.arch.lock;

import java.util.concurrent.TimeUnit;

import com.nx.arch.addon.lock.NxLock;
import com.nx.arch.addon.lock.NxLockClient;
import com.nx.arch.addon.lock.exception.LockException;

public class Test6 {

	private static NxLockClient LCOK_CLIENT;
	private static String lockName = "chenyang_lock";
	private static String testAddr = "http://192.168.188.50:12379";
	
	static {
		try {
			LCOK_CLIENT = NxLock.factory().etcdSolution().timeout(20).connectionString(testAddr.split(","))
					.clusterName("monitor-server").build();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	static class BlockThread extends Thread {
		@Override
		public void run() {
		    while(true) {
    			try {
    				NxLock lock = LCOK_CLIENT.newLock(lockName);
    				if(lock.acquire(5)) {
    					System.out.println("BlockThread get lock, time to sleep...");
    					TimeUnit.SECONDS.sleep(8);
    					lock.release();
    					System.out.println("BlockThread release the lock");
    				} else {
//    					System.err.println("BlockThread can not get lock, will exit");
    				}
    			} catch (LockException e) {
    				e.printStackTrace();
    			} catch (InterruptedException e) {
    				e.printStackTrace();
    			}
		    }
		}
	}
	
	static class OtherThread extends Thread {
		@Override
		public void run() {
		    while(true) {
    			try {
    				NxLock lock = LCOK_CLIENT.newLock(lockName);
    				if(lock.acquire(5)) {
    					System.out.println("OtherThread get lock, continue...");
    					lock.release();
    					System.out.println("OtherThread release the lock");
    				} else {
//    					System.out.println("OtherThread can not get lock");
    				}
//    				TimeUnit.SECONDS.sleep(1);
    			} catch (Throwable e) {
    				e.printStackTrace();
    			}
		    }
		}
	}

	public static void main(String[] args) throws Exception {
		new BlockThread().start();
		TimeUnit.SECONDS.sleep(2);
		new OtherThread().start();
		
		TimeUnit.DAYS.sleep(1);
	}
}
