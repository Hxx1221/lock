package com.nx.arch.lock;

import com.nx.arch.addon.lock.NxLock;
import com.nx.arch.addon.lock.NxLockClient;

public class Test5 {
	public static void main(String[] args) throws Exception {
		//
		NxLockClient nxClient = NxLock.factory().etcdSolution()
				  .connectionString("http://etcd1.Lock.nxinc.com:2379","http://etcd2.Lock.nxinc.com:2379","http://etcd3.Lock.nxinc.com:2379","http://etcd4.Lock.nxinc.com:2379","http://etcd5.Lock.nxinc.com:2379")
				  .clusterName("monitor-server2")
				  .build();
		for(int i = 0 ;i <9 ;i++){
			new Thread(new MyRun(i)).start();
		}
		NxLock lock = nxClient.newLock("firstLock23");
		System.err.println("22222222aaa"+lock.acquire(10,5,5));
		Thread.sleep(10*1000);
		lock.release();
	}
	
	public static class MyRun implements Runnable{
		int countIndex = 0;
    	public MyRun(int countIndex){
			this.countIndex = countIndex ;
		}    
        @Override
        public void run() {  
        	NxLockClient nxClient;
			try {
				nxClient = NxLock.factory().etcdSolution()
						  .connectionString("http://etcd1.Lock.nxinc.com:2379","http://etcd2.Lock.nxinc.com:2379","http://etcd3.Lock.nxinc.com:2379","http://etcd4.Lock.nxinc.com:2379","http://etcd5.Lock.nxinc.com:2379")
						  .clusterName("monitor-server2")
						  .build();
				NxLock lock = nxClient.newLock("firstLock23");
				System.err.println("monitor-server-"+countIndex+"："+lock.acquire(10));
				Thread.sleep(15*1000);
				lock.release();
			} catch (Exception e) {
				e.printStackTrace();
			}
        }
	}
}
