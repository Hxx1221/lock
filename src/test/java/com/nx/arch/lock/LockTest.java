package com.nx.arch.lock;

import com.nx.arch.addon.lock.NxLock;
import com.nx.arch.addon.lock.NxLockClient;
import com.nx.arch.addon.lock.exception.LockException;

import net.sourceforge.groboutils.junit.v1.MultiThreadedTestRunner;
import net.sourceforge.groboutils.junit.v1.TestRunnable;

public class LockTest {
    public static void main(String[] args)
        throws Exception {
        NxLockClient nxClient = NxLock.factory().redisSolution().build();
        NxLock lock = nxClient.newLock("firstLock16");
        System.err.println("22222222aaa：" + lock.acquire(10));
        Thread.sleep(1000);
        lock.release();
        
        nxClient.close();
        NxLock lock2 = nxClient.newLock("firstLock17");
        System.err.println("33333aaa" + lock2.acquire(10));
    }
    
    @org.junit.Test
    public void sychLock() {
        System.out.println(Thread.currentThread().getName() + "------------------------------");
        TestRunnable runner = new TestRunnable() {
            @Override
            public void runTest() throws Exception {
                NxLockClient nxClient = NxLock.factory().redisSolution().build();
                try (NxLock lock = nxClient.newLock("firstLock16")) {
                    if (lock.acquire(20, 1, 10)) {
                        System.out.println(Thread.currentThread().getName().concat("获得锁"));
                        try {
                            Thread.sleep(20);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (LockException e1) {
                    e1.printStackTrace();
                }
            }
        };
        // 开13，23，43个线程进行测试
        int runnerCount = 15;
        TestRunnable[] trs = new TestRunnable[runnerCount];
        for (int i = 0; i < runnerCount; i++) {
            trs[i] = runner;
        }
        
        MultiThreadedTestRunner mttr = new MultiThreadedTestRunner(trs);
        try {
            mttr.runTestRunnables();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        
    }
    
    public static class MyRun implements Runnable {
        int countIndex = 0;
        
        public MyRun(int countIndex) {
            this.countIndex = countIndex;
        }
        
        @Override
        public void run() {
            NxLockClient nxClient;
            try {
                nxClient = NxLock.factory().etcdSolution().connectionString("http://10.132.6.41:2379", "http://10.132.78.14:2379", "http://10.132.78.15:2379").clusterName("monitor-server").build();
                NxLock lock = nxClient.newLock("firstLock16");
                System.err.println("monitor-server-" + countIndex + "：" + lock.acquire(20));
                Thread.sleep(1000 * 10);
                lock.release();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
