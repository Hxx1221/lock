package com.nx.arch.addon.lock.util;

import java.util.concurrent.TimeUnit;

import com.nx.arch.addon.lock.client.EtcdClient;
import com.nx.arch.addon.lock.client.EtcdResponse;
import com.nx.arch.addon.lock.exception.LockException;

/**
 * @类名称 HBRenewTask.java
 * @类描述 续租线程
 * @作者  庄梦蝶殇 linhuaichuan@naixuejiaoyu.com
 * @创建时间 2020年3月28日 下午3:46:20
 * @版本 1.0.0
 *
 * @修改记录
 * <pre>
 *     版本                       修改人 		修改日期 		 修改内容描述
 *     ----------------------------------------------
 *     1.0.0 		庄梦蝶殇 	2020年3月28日             
 *     ----------------------------------------------
 * </pre>
 */
public class RenewTask extends Thread {
    public volatile boolean isRunning = true;
    
    private EtcdClient client;
    
    /**
     * 锁名
     */
    private String key;
    
    /**
     * 锁值
     */
    private String name;
    
    /**
     * 过期时间
     */
    private int ttl;
    
    public RenewTask(EtcdClient client, String key, String name, int ttl) {
        this.client = client;
        this.key = key;
        this.name = name;
        this.ttl = ttl;
    }
    
    @Override
    public void run() {
        while (isRunning) {
            try {
                // 1、续租，刷新值
                EtcdResponse result = client.casVal(key, name, name, ttl <= 0 ? 10 : ttl);
                if (result.isError()) {
                    close();
                }
                // 2、三分之一过期时间续租
                TimeUnit.SECONDS.sleep((ttl <= 0 ? 10 : ttl) / 3);
            } catch (InterruptedException e) {
                close();
            } catch (LockException e) {
                close();
            }
        }
    }
    
    public void close() {
        isRunning = false;
    }
}
